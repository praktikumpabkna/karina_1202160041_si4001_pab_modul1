package com.example.karina_1202160041_si4001_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText alas, tinggi;
    private Button check;
    private TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        alas = findViewById(R.id.alas);
        tinggi = findViewById(R.id.tinggi);
        check = findViewById(R.id.buttoncheck);
        result = findViewById(R.id.result);
    }


    public void cek(View view) {
        String inputalas = alas.getText().toString();
        String inputtinggi = tinggi.getText().toString();

        Integer hasilalas = Integer.parseInt(inputalas);
        Integer hasiltinggi = Integer.parseInt(inputtinggi);

        Integer hitung = hasilalas * hasiltinggi;
        result.setText(Integer.toString(hitung));
    }
}
